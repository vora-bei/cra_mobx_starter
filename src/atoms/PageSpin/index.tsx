import { Spin } from 'antd';
import React from 'react';
const SpinPage: React.FC = () => {
    return <Spin size = "large"/>
}
export default SpinPage;