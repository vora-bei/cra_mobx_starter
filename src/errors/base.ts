export class  ApplicationError extends Error {
    public code?: string;
    constructor(serverError: {message: string, code?: string, status?: string, name: string}){
        super();
        this.code = serverError.code || serverError.status;
        this.name = serverError.name;
        this.message = serverError.message;
    }
}