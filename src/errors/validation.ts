import { ApplicationError } from "./base";

export type Validations = {[name:string]: {path: string, errors: string[]}}
export class  ValidationError extends ApplicationError {
    public validations: Validations = {};
    constructor(serverError: {message: string, code?: string, status?: string, name: string, validations: Validations}){
        super(serverError);
        this.validations = serverError.validations;
    }
}