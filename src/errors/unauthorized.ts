import { ApplicationError } from "./base";

export class UnauthorizedError extends ApplicationError {
    constructor(serverError: {message: string, code?: string, status?: string, name: string}){
        super(serverError);
    }
}