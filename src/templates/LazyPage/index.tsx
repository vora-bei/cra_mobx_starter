import React, { Suspense, ReactNode } from 'react';
import SpinPage from 'atoms/PageSpin'
const LazyComponent: React.FC<{children?: ReactNode }> = ({children}) => (
  <Suspense fallback={<SpinPage />}>
    {children}
  </Suspense>
);
export default LazyComponent;