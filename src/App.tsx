import React  from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { ConfigProvider } from 'antd';
import ru_RU from 'antd/es/locale/ru_RU';
import ruMessages from './locales/ru.json';
import { IntlProvider, useIntl } from 'react-intl';
import LazyPage from "templates/LazyPage"
import { Provider, rootStore } from "stores/root"
import './App.css';
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;
const App: React.FC = () => {
  const ProductOffering = React.lazy<React.FC>(() => import("pages/productOffering"));
  const Main = React.lazy<React.FC>(() => import("pages/main"));
  return (
      <ConfigProvider locale={ru_RU}>
        <IntlProvider locale={"ru"} defaultLocale={"ru"} messages={ruMessages}>
          <Provider value={rootStore}>
            <Layout className="layout">
              <Header>
                <div className="logo" />
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    style={{ lineHeight: '64px' }}
                >
                  <Menu.Item key="1">nav 1</Menu.Item>
                  <Menu.Item key="2">nav 2</Menu.Item>
                  <Menu.Item key="3">nav 3</Menu.Item>
                </Menu>
              </Header>
              <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                  <Breadcrumb.Item>Home</Breadcrumb.Item>
                  <Breadcrumb.Item>List</Breadcrumb.Item>
                  <Breadcrumb.Item>App</Breadcrumb.Item>
                </Breadcrumb>
                <Router>
                  <Switch>
                    <Route exact path="/productsOffering">
                      <LazyPage>
                        <ProductOffering />
                      </LazyPage>
                    </Route>
                    <Route exact path="/">
                      <LazyPage>
                        <Main />
                      </LazyPage>
                    </Route>
                  </Switch>
                </Router>
              </Content>
              <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
          </Provider>
        </IntlProvider>
      </ConfigProvider>
  );
}
export default App;
