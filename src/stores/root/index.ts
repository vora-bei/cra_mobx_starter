
import { useContext, createContext } from "react";
import { types, Instance, onSnapshot, cast } from "mobx-state-tree";

import { ProductOfferingList } from "../productOffering";
import { Auth } from "../auth";
import { Request } from "../request";

const RootModel = types.model({
    request: Request,
    productOfferingList: ProductOfferingList,
    auth: Auth,
});

export const rootStore = RootModel.create({
    productOfferingList: {},
    auth: {},
    request: {},
});

onSnapshot(rootStore, snapshot => console.log("Snapshot: ", snapshot));

export type RootInstance = Instance<typeof RootModel>;
const RootStoreContext = createContext<null | RootInstance>(null);

export const Provider = RootStoreContext.Provider;
export function useMst() {
    const store = useContext(RootStoreContext);
    if (store === null) {
        throw new Error("Store cannot be null, please add a context provider");
    }
    return store;
}