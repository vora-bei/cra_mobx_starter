import {getParent, Instance, SnapshotIn, types, } from "mobx-state-tree";
import {runInAction} from "mobx";
import { getRequest } from "../request"
import { Form } from "../common"
import { ValidationError } from "../../errors"
let rules = { name : 'required', password : 'required' };

export const Login = types.model({
    name: types.string,
    password: types.string,
    form: types.optional(Form, {}),
}).actions(self => ({
    change(name: "name"| "password", value: string) {
           self[name] = value;
           self.form.validateClient(self, rules);
           self.form.setDirty(name);
        },
    async signIn() {
        const request = getRequest(self);
        try {
            self.form.validateClient(self, rules);
            if(self.form.hasErrors){
                self.form.setDirtyAll();
            }
            const jwt = await request.exec("/signIn", { name: self.name, password: self.password });
            const auth = getParent<Instance<typeof Auth>>(self);
            auth.setJWT({access: jwt});
            auth.setIsAuth(true);
            self.name = "";
            self.password = "";
            self.form.fields.clear();
        } catch(e){
            if( e instanceof ValidationError ){
                runInAction(()=>{
                    self.form.validateServer(e.validations);
                })
            }
        }
    },
}));
export const Csrf = types.model({
    value: types.maybe(types.string)
});
export const Register = types.model({
    name: types.string,
    password: types.string,
    repeatPassword: types.string,
});
export const Jwt = types.model({
    access: types.maybeNull(types.string),
    refresh: types.maybeNull(types.string),
});
export const Auth = types.model({
    login : types.optional(Login, { name: "", password: "" }),
    register : types.optional(Register, { name: "", password: "", repeatPassword: "" }),
    csrf: types.optional(Csrf, {}),
    jwt: types.optional(Jwt, {}),
    isAuth: types.optional(types.boolean, true),
}).actions(self => ({
    getCsrf(){
        return self.csrf.value;
    },
    setCsrf(value: string){
        self.csrf.value = value
    },
    setIsAuth(auth: boolean){
        self.isAuth = auth;
    },
    async loadCsrf(){
        //if(!self.csrf.value){
       //     this.setCsrf(await getRequest(self).execHandler("/csrf"))
        //}
        return self.csrf;
    },
    async loadJWT(): Promise<Instance<typeof Jwt>>{
        try {
            if(!self.jwt.access){
                const access = localStorage.getItem("jwtAccess");
                const refresh = localStorage.getItem("jwtRefresh");
                this.setJWT({ access, refresh });
            }
            return self.jwt;
        } catch (e) {
            console.error(e);
        }
        return self.jwt;
    },
    setJWT(jwt: SnapshotIn<typeof Jwt> | Instance<typeof Jwt>){
        self.jwt = Jwt.create(jwt);
    },
}));