import {types} from "mobx-state-tree";

export const ProductOfferingPrice = types.model({});
/*
[
                    {
                        bundledPopRelationship: types.array(Href),
                        constraint: [
                            {
                                href: types.string,
                                id: types.string,
                                name: types.string,
                                version: types.string
                            }
                        ],
                        description: types.string,
                        href: types.string,
                        id: types.string,
                        isBundle: types.boolean,
                        lastUpdate: "2020-02-19T13:05:59.823Z",
                        lifecycleStatus: types.string,
                        name: types.string,
                        percentage: types.number,
                        place: [
                            {
                                address: types.string,
                                geoLocationUrl: types.string,
                                href: types.string,
                                id: types.string,
                                name: types.string,
                                role: types.string
                            }
                        ],
                        popRelationship: [
                            {
                                "@type": types.string,
                                href: types.string,
                                id: types.string,
                                name: types.string
                            }
                        ],
                        price: {
                            unit: types.string,
                            value: 0
                        },
                        priceType: types.string,
                        pricingLogicAlgorithm: [
                            {
                                description: types.string,
                                href: types.string,
                                id: types.string,
                                name: types.string,
                                plaSpecId: types.string,
                                validFor: ValidFor
                            }
                        ],
                        prodSpecCharValueUse: types.array(ProdSpecCharValueUse),
                        productOfferingTerm: [
                            {
                                description: types.string,
                                duration: {
                                    amount: types.number,
                                    units: types.string
                                },
                                name: types.string,
                                validFor: ValidFor
                            }
                        ],
                        recurringChargePeriodLength: types.number,
                        recurringChargePeriodType: types.string,
                        tax: [
                            {
                                taxAmount: {
                                    unit: types.string,
                                    value: 0
                                },
                                taxCategory: {
                                    unit: types.string,
                                    value: 0
                                },
                                taxRate: 0
                            }
                        ],
                        unitOfMeasure: {
                            unit: types.string,
                            value: 0
                        },
                        validFor: ValidFor,
                        version: types.string
                    }
                ]
 */