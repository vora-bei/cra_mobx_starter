import {types} from "mobx-state-tree";
import { Href, ValidFor } from "../common";

export const ProductSpecCharacteristicValue = types.model({
    "@type": types.string,
    isDefault: types.boolean,
    rangeInterval: types.string,
    regex: types.string,
    unitOfMeasure: types.string,
    validFor: ValidFor,
    value: types.string,
    valueFrom: types.string,
    valueTo: types.string,
    valueType: types.string
});
export const ProdSpecCharValueUse = types.model({
    description: types.string,
    maxCardinality: types.number,
    minCardinality: types.number,
    name: types.string,
    productSpecCharacteristicValue: types.array(ProductSpecCharacteristicValue),
    productSpecification: Href,
    validFor: ValidFor,
    valueType: types.string
});