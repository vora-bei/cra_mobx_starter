import {
    types,
    Instance,
    cast,
    SnapshotIn,
    destroy,
    getRoot, IAnyStateTreeNode,
} from "mobx-state-tree";
import { ProdSpecCharValueUse } from "../prodSpecCharValue"
import { ProductOfferingPrice } from "../productOfferingPrice"
import { getRequest } from "../request"
import { Href, Attachment, ValidFor, Pagination } from "../common"

export const BundledProductOffering = types.model({
        bundledProductOffering: types.model({
            numberRelOfferDefault: types.number,
            numberRelOfferLowerLimit: types.number,
            numberRelOfferUpperLimit: types.number
        }),
        href: types.string,
        id: types.string,
        lifecycleStatus: types.string,
        name: types.string
    });

export const ProductOfferingForm = types.model({
    agreement: types.array(Href),
    attachment: types.array(Attachment),
    bundledProductOffering: types.array(BundledProductOffering),
    category: types.array(Href),
    channel: types.array(Href),
    description: types.string,
    href: types.string,
    id: types.string,
    isBundle: types.boolean,
    isSellable: types.boolean,
    lastUpdate: types.string,
    lifecycleStatus: types.string,
    marketSegment: types.array(Href),
    name: types.string,
    place: types.array(types.model({
            address: types.string,
            geoLocationUrl: types.string,
            href: types.string,
            id: types.string,
            name: types.string,
            role: types.string
        })
    ),
    prodSpecCharValueUse: types.array(ProdSpecCharValueUse),
    productOfferingPrice: types.array(ProductOfferingPrice),
    productOfferingTerm: types.array(types.model(
        {
            "@schemaLocation": types.string,
            "@type": types.string,
            description: types.string,
            duration: types.model({
                amount: types.number,
                units: types.string
            }),
            name: types.string,
            validFor: ValidFor
        })
    ),
    productSpecification: Href,
    resourceCandidate: Href,
    serviceCandidate: Href,
    serviceLevelAgreement: Href,
    validFor: ValidFor,
    version: types.string,
});

export const ProductOfferingItem = types.model({
                agreement: types.array(Href),
                attachment: types.array(Attachment),
                category: types.array(Href),
                channel: types.array(Href),
                description: types.string,
                href: types.string,
                id: types.string,
                isBundle: types.boolean,
                isSellable: types.boolean,
                lastUpdate: types.string,
                lifecycleStatus: types.string,
                marketSegment: types.array(Href),
                name: types.string,
                productSpecification: Href,
                resourceCandidate: Href,
                serviceCandidate: Href,
                serviceLevelAgreement: Href,
                validFor: ValidFor,
                version: types.string,
});
export const ProductOfferingListFilter = types.model({
    category: types.optional(types.array(Href),[]),
    channel: types.optional(types.array(Href),[]),
    description: types.maybeNull(types.string),
    id: types.maybeNull(types.string),
    isBundle: types.maybeNull(types.boolean),
    isSellable: types.maybeNull(types.boolean),
    lastUpdate: types.maybeNull(types.string),
    lifecycleStatus: types.maybeNull(types.string),
    marketSegment: types.optional(types.array(Href),[]),
    name: types.maybeNull(types.string),
    productSpecification: types.maybeNull(Href),
    resourceCandidate: types.maybeNull(Href),
    serviceCandidate: types.maybeNull(Href),
    serviceLevelAgreement: types.maybeNull(Href),
    version: types.maybeNull(types.string),
});
export const ProductOfferingList = types
    .model({
        filter: types.optional(ProductOfferingListFilter, {}),
        items: types.optional(types.array(ProductOfferingItem), []),
        pagination: types.optional(Pagination, () => Pagination.create({total: 0, count: 10, offset: 0})),
    })
    .actions(self => ({
        async load(){
            const response = await getRequest(self).exec('/productOffering', {});
            this.onLoad(response);
        },
        onLoad(json: any){
            self.items = cast(json);
        },
        add(
            cartItem: SnapshotIn<typeof ProductOfferingItem> | Instance<typeof ProductOfferingItem>
        ) {
            self.items.push(cartItem);
        },
        remove(item: SnapshotIn<typeof ProductOfferingItem>) {
            destroy(item);
        }
    }))
    .views(self => ({
        get totalItems() {
            return self.items.length;
        }
    }));