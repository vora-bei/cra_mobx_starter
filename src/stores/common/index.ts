import {types, cast} from "mobx-state-tree";
import Validator from 'validatorjs';
import { Validations } from '../../errors';
import ru from '../../locales/ru.json';
export const Href = types.model({
    '@type': types.optional(types.string, ""),
    '@referredType': types.optional(types.string, ""),
    href: types.string,
    id: types.string,
    name: types.string,
    version: types.optional(types.string, ""),

});
export const ValidFor = types.model({
    endDateTime: types.string,
    startDateTime: types.string
});
export const Attachment = types.model({
    description: types.string,
    href: types.string,
    id: types.string,
    mimeType: types.string,
    type: types.string,
    url: types.string,
    validFor :ValidFor
})
export const Pagination = types.model({
   total: types.number,
   count: types.number,
   offset: types.number
});

export const FormInput = types.model({
    path: types.string,
    validationStatus: types.optional(types.enumeration("Status", ["success", "warning", "error", "validating", ""]),""),
    errors: types.array(types.string),
    dirty: types.optional(types.boolean, false),
});


export const Form = types.model({
    fields: types.map(FormInput),
    hasErrors: types.optional(types.boolean, false),
}).actions(self => ({
    clearError(){
        self.fields.forEach((v, key)=>{
            const field = self.fields.get(key);
            if(field) {
                field.errors = cast([]);
                field.validationStatus = "";
            }
        });
        self.hasErrors = false;
    },
    setDirtyAll(){
        self.fields.forEach((v, key)=>{
            const field = self.fields.get(key);
            if(field) {
                field.dirty = true;
            }
        });
        self.hasErrors = false;
    }

})).actions(self => ({
    validateClient(form: any, rules: any) {
        Validator.useLang("ru");
        const validator = new Validator(form, rules);
        validator.setAttributeNames(ru);
        validator.check();
        self.clearError();
        for (const [key, errors] of Object.entries(validator.errors.errors)){
            const field = self.fields.get(key) ||
                FormInput.create(
                    {
                        path: key,
                        errors,
                    });
            self.fields.set(key, field);
            field.validationStatus = errors.length ? "error" : "";
            field.errors = cast(errors);
            if(errors.length){
                self.hasErrors = true;
            }
        }
    },
    validateServer(validation: Validations)  {
        self.clearError();
        for (const [key, value] of Object.entries(validation)){
            const field = self.fields.get(key)|| FormInput.create({path: key, errors: value.errors})
            self.fields.set(key, field);
            field.validationStatus = value.errors.length ? "error" : "";
            field.errors = cast(value.errors);
        }
    },
    setDirty(path: string){
        const field = self.fields.get(path) || FormInput.create({path: path, errors: []});
        self.fields.set(path, field);
        field.dirty = true;
    },
})).views(self => ({
    field(path: string){
        return self.fields.get(path) || FormInput.create({path: path, errors: []});
    },
    fieldValidSt(path: string){
        const field = self.fields.get(path) || FormInput.create({path: path, errors: []});
        return field.dirty ? field.validationStatus : "";
    },
    fieldHelp(path: string, locale: string){
        const field = self.fields.get(path) || FormInput.create({path: path, errors: []});
        return field.dirty ? field.errors.join("\n") : "";
    },
}));