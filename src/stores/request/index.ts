import {
    types,
    flow, IAnyStateTreeNode, Instance, getRoot
} from "mobx-state-tree";
import { Auth } from "../auth";
import { ApplicationError } from "../../errors";

interface IRequestOptions {
    spinnerKey: string,
    headers: any;
}
export const Spinner = types.model({
    id: types.identifier,
    count: types.optional(types.number, 0),
    loading: types.boolean
})
export const getRequest = (self: IAnyStateTreeNode): Instance<typeof Request> => {
    const root: any = getRoot(self);
    return root.request;
}
export const getAuth = (self: IAnyStateTreeNode): Instance<typeof Auth> => {
    const root: any = getRoot(self);
    return root.auth;
}
export const Request = types
    .model({
        spinners: types.optional(types.map(Spinner), {}),
    })
    .actions(self => ({
        async execHandler(url: string, params: any = {}, options: IRequestOptions = { spinnerKey: "global", headers: {} } ){
            try {
                this.onStart(url, params, options);
                const response = await fetch(url, {
                    headers: {
                        'Content-Type': 'application/json',
                        'accept': 'application/json',
                        ...options.headers,
                    },
                });
                let json: any = {};
                try {
                    json = await response.json();
                } catch (e) {
                    const message = await response.text();
                    throw new ApplicationError({name: "500", message, code: String(response.status)})
                }
                if (!response.ok) { throw new ApplicationError(json) }
                this.onSuccess(json, options);
                return json;
            } catch (e) {
                this.onFail(options);
                throw e;
            }
        },
        async exec(url: string, params: any = {}, options: IRequestOptions = { spinnerKey: "global", headers: {} } ){
            try {
                const auth = getAuth(self);
                const csrf = await auth.loadCsrf();
                const jwt = await auth.loadJWT();
                return await this.execHandler(url, params, {
                    ...options,
                    headers: { Authorization: `Bearer ${jwt.access}`, csrf: csrf.value }
                })
            } catch (e) {
                if(e instanceof ApplicationError){
                    if(e.code === "401"){
                        getAuth(self).setIsAuth(false)
                    }
                }
                throw new ApplicationError(e);
            }
        },
        onStart(url: string, params: any, options: IRequestOptions){
            const spinner = self.spinners.get(options.spinnerKey) || Spinner.create({
                id: options.spinnerKey,
                count: 0,
                loading: false,
            });
            self.spinners.set(options.spinnerKey, spinner)
            spinner.count = spinner.count + 1;
            spinner.loading = true;
        },
        onFail(options: IRequestOptions){
            this.spinnerDecr(options);
        },
        onSuccess(json: any, options: IRequestOptions){
            this.spinnerDecr(options);
        },
        spinnerDecr(options: IRequestOptions){
            const spinner = self.spinners.get(options.spinnerKey);
            if(spinner){
                spinner.count--;
                spinner.loading = spinner.count!==0;
                spinner.id = options.spinnerKey;
                self.spinners.set(options.spinnerKey, spinner)
            }
        }
    }));