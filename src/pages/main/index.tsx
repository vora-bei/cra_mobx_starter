
import React, {useCallback} from 'react';
import 'antd/dist/antd.css';
import { Form, Icon, Input, Button, Checkbox } from 'antd';

import { useObserver } from "mobx-react-lite"
import { useMst } from "stores/root";
import { useIntl } from 'react-intl';


// register
// modal
// https://codesandbox.io/s/n9c45
interface IProps {
}
const Home: React.FC<IProps> = React.memo((props: IProps) => {
    const { auth } = useMst();
    const { login } = auth;
    const {formatMessage: f, locale} = useIntl();
    const onChangeName = useCallback((event) => {
        login.change("name", event.target.value);
    },[]);
    const onChangePassword = useCallback((event) => {
        login.change("password", event.target.value);
    },[]);
    const signIn = useCallback((event) => {
        event.preventDefault()
        login.signIn();
    },[]);
    return useObserver(() =>
        <Form onSubmit={() => {}} className="login-form">
            <Form.Item
                label={f({id: "auth.login"})}
                validateStatus={login.form.fieldValidSt("name")}
                help={login.form.fieldHelp("name", locale)}
            >
                    <Input
                        value={auth.login.name}
                        onChange={onChangeName}
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder={f({id: "auth.login"})}
                    />
            </Form.Item>
            <Form.Item
                label={f({id: "auth.password"})}
                validateStatus={login.form.fieldValidSt("password")}
                help={login.form.fieldHelp("password", locale)}
            >
                <Input
                    value={auth.login.password}
                    onChange={onChangePassword}
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder={f({id: "auth.password"})}
                />
            </Form.Item>
            <Button onClick={signIn} type="primary" htmlType="submit" className="login-form-button">
                Log in
            </Button>
            Or <a href="">register now!</a>
        </Form>
    )
})
export default Home;