import React from "react";
import { useObserver } from "mobx-react-lite"
import { useMst } from "stores/root";
import { Table, Divider, Tag } from 'antd';
import styles from './index.module.css';


// intl
// login
// register
// modal
// https://codesandbox.io/s/n9c45
const Home: React.FC = React.memo(() => {
    const { productOfferingList } = useMst();
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
    ];
    return useObserver(() =>
        <div
            onClick={() => productOfferingList.load()}
            className={styles.page}>
            <Table dataSource={[...productOfferingList.items]} columns={columns} />
        </div>
    )
})
export default Home;